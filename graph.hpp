#pragma once
#include <boost/variant.hpp>
#include <vector>

using value = boost::variant<int, float, std::vector<float>>;

struct audio_callback
{
    float** inputs{};
    float** outputs{};
    int num_inputs{};
    int num_outputs{};
    int buffer_size{};
    int sample_rate{};
    void operator()(float** ins, float** outs, int n_in, int n_out, uint32_t frames, int rate)
    {
        inputs = ins;
        outputs = outs;
        num_inputs = n_in;
        num_outputs = n_out;
        buffer_size = frames;
        sample_rate = rate;

        // TODO
    }
};

class graph_node
{
public:
    virtual ~graph_node()
    {

    }
    virtual void run(int sample_rate, int buffer_size) = 0;

    std::vector<value> inputs;
    std::vector<value> outputs;
};

class adc final : public graph_node
{
public:
    adc(const audio_callback& cb)
        : audio{cb}
    {
        outputs.emplace_back(std::vector<float>{});
    }

    void run(int sample_rate, int buffer_size) override
    {
        // Pour simplifier on ne va copier que le premier canal dans la sortie de ce noeud
        if(audio.num_inputs >= 1)
        {
            auto& data = boost::get<std::vector<float>>(outputs[0]);
            data.assign(audio.inputs[0], audio.inputs[0] + buffer_size);
        }
    }

private:
    const audio_callback& audio;
};

class dac final : public graph_node
{
public:
    dac(const audio_callback& cb)
        : audio{cb}
    {
        inputs.emplace_back(std::vector<float>{});
    }

    void run(int sample_rate, int buffer_size) override
    {
        // Idem, on fait tout en mono
        if(audio.num_outputs >= 1)
        {
            // L'entrée de ce noeud va sur la sortie de la carte son
            const auto& data = boost::get<std::vector<float>>(inputs[0]);
            std::copy_n(data.begin(), buffer_size, audio.outputs[0]);
        }
    }

private:
    const audio_callback& audio;
};

class audio_gain final : public graph_node
{
public:
    audio_gain()
    {
        inputs.emplace_back(std::vector<float>{});
        inputs.emplace_back(float{0.5});
        outputs.emplace_back(std::vector<float>{});
    }

    void run(int sample_rate, int buffer_size) override
    {
        const auto& in = boost::get<std::vector<float>>(inputs[0]);
        auto gain = boost::get<float>(inputs[1]);
        auto& out = boost::get<std::vector<float>>(outputs[0]);

        // On applique le gain sur la sortie
        out.resize(in.size());
        for(std::size_t i = 0; i < in.size(); i++)
        {
            out[i] = in[i] * gain;
        }
    }
};



static void test_graph()
{
    audio_callback c;
    adc in{c};
    audio_gain g;
    dac out{c};

    float input[64]{1, 0.5, 1, 0};
    float output[64]{};
    float* ins[] = {input};
    float* outs[] = {output};

    c(ins, outs, 1, 1, 64, 44100);

    in.run(44100, 64);
    g.inputs[0] = in.outputs[0];
    g.run(44100, 64);
    out.inputs[0] = g.outputs[0];
    out.run(44100, 64);

    assert(output[0] == 0.5);
    assert(output[1] == 0.25);
    assert(output[2] == 0.5);
    assert(output[3] == 0.);
}
