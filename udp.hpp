#pragma once
#include <asio.hpp>

using namespace asio;
using namespace asio::ip;

class udp_client
{
public:
  udp_client(std::string host, uint16_t port)
      : m_resolver{m_service}
      , m_query{udp::v4(), host, std::to_string(port)}
      , m_endpoint{*m_resolver.resolve(m_query)}
      , m_socket{m_service}
  {
    m_socket.open(udp::v4());
  }

  void send(const char* data, int size)
  {
    m_socket.send_to(asio::buffer(data, size), m_endpoint);
  }

private:
  io_service m_service;
  udp::resolver m_resolver;
  udp::resolver::query m_query;

  udp::endpoint m_endpoint;
  udp::socket m_socket;
};

template <typename OnMessage>
class udp_server
{
public:
  udp_server(OnMessage m, uint16_t port)
      : m_socket{m_service, udp::endpoint{udp::v4(), port}}, m_cb{std::move(m)}
  {
    start_receive();
  }

  io_service& service()
  {
    return m_service;
  }

private:
  void start_receive()
  {
    m_socket.async_receive_from(
        buffer(m_buf), m_endpoint,
        [=](const asio::error_code& error, std::size_t size) {
          if (!error || error == error::message_size)
          {
            m_cb(m_buf.data(), size);
            start_receive();
          }
        });
  }

  io_service m_service;
  udp::socket m_socket;
  udp::endpoint m_endpoint;
  std::array<char, 16384> m_buf;
  OnMessage m_cb;
};



static void test_udp()
{
    using namespace std::literals;
    std::atomic_bool stop{false};
    std::thread receiver{[&] {
        struct message_receiver
        {
            asio::io_service* service{};
            void operator()(const char* data, std::size_t sz)
            {
                std::cerr << data << "\n";
                if(sz >= 4)
                {
                    if(data[0] == 'S' && data[1] == 'T' && data[2] == 'O' && data[3] == 'P')
                    {
                        service->stop();
                    }
                }
            }
        };

        message_receiver m;
        udp_server<message_receiver> server{m, 12345};
        m.service = &server.service();
        server.service().run();
    }};

    std::thread sender{[&] {
        udp_client client("127.0.0.1", 12345);
        while(!stop) {
          client.send("/hello\0\0", 8);
          std::this_thread::sleep_for(100ms);
        }
        client.send("STOP", 4);
    }};

    getchar();
    stop = true;
    sender.join();
    receiver.join();
}

